const images = {
  gardenia: require('./gardenia.jpg'),
  massimo: require('./massimo.png'),
};

export default images;
