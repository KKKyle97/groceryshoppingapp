import React, {Component} from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import {ListItem} from 'react-native-elements';
import {openDatabase} from 'react-native-sqlite-storage';

var db = openDatabase({name: 'local_db.db', createFromLocation: 1});

export default class SubCategoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subcategory: [],
      titles: this.props.route.params.title,
      id: this.props.route.params.id,
    };

    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM sub_category where main_category_id = ?',
        [this.state.id],
        (tx, results) => {
          var temp = [];
          for (let i = 0; i < results.rows.length; ++i) {
            temp.push(results.rows.item(i));
          }
          this.setState({
            subcategory: temp,
          });
        },
      );
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.subcategory}
          showsVerticalScrollIndicator={false}
          renderItem={({item}) => (
            <ListItem
              leftAvatar={{
                source: {uri: `data:image/jpeg;base64,${item.image}`},
              }}
              title={item.name}
              onPress={() => {
                this.props.navigation.navigate('ItemList', {
                  title: item.name,
                  id: item.id,
                });
              }}
              bottomDivider
              chevron
            />
          )}
          keyExtractor={(item) => {
            item.id.toString();
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  item: {
    justifyContent: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomWidth: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemTitle: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  itemDate: {
    fontSize: 18,
  },
  header_style: {
    width: '100%',
    height: 60,
    backgroundColor: '#3369FF',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
