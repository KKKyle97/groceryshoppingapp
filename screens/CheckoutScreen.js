import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, Text, Alert } from 'react-native';
import { ListItem } from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';
import CustomizedButton, { SignInButton } from '../components/CustomizedButton';

class CheckoutScreen extends Component {

  constructor() {
    super();
    this.firestoreRef = firestore().collection('delivery');
    this.firestoreRef1 = firestore().collection('baskets');
    this.state = {
      isLoading: true,
      deliveryArr: []
    };
  }

  componentDidMount() {
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  getCollection = (querySnapshot) => {
    const deliveryArr = [];
    querySnapshot.forEach((res) => {
      const { name, contact, address, paymentMethod } = res.data();
      deliveryArr.push({
        key: res.id,
        res,
        name,
        contact,
        paymentMethod,
        address,
      });
    });
    this.setState({
      deliveryArr,
      isLoading: false,
   });
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
    return (
      <ScrollView style={styles.container}>
        {
          this.state.deliveryArr.map((item, i) => {
            if(i == 0){
              return (
                <View key={i}>
                <ListItem             
                  chevron
                  bottomDivider
                  title={<Text>Name</Text>}
                  subtitle={item.name}
                  
                />
                <ListItem
                  chevron
                  bottomDivider
                  title={<Text>ContactInfo</Text>}
                  subtitle={item.contact}           
                />
                <ListItem
                  chevron
                  bottomDivider
                  title={<Text>Address</Text>}
                  subtitle={item.address}
                />
                 <ListItem
                  chevron
                  bottomDivider
                  title={<Text>Payment Method</Text>}
                  subtitle={item.paymentMethod}
                />
                </View>
              )
            }
          })
        }
        <View style={styles.stickToBottom}>
          <CustomizedButton title="Edit Delivery Info" onPress={()=> {this.props.navigation.navigate('Delivery')}} />
        </View>
        <View style={styles.stickToBottom}>
          <SignInButton title="Confirm Checkout" onPress={()=> 
          {
            Alert.alert('Check out successful! Check your orders in Orders.')
            firestore().collection('baskets').get().then(res =>  {
              res.forEach(doc => {
                  this.firestoreRef1.doc(doc.id).delete()
                  .catch(error => {
                      console.log(error)
                  })
              })
          })
          }} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingBottom: 22
  },
  
  stickToBottom: {
    flex: 1,
    justifyContent: 'flex-end',
  },

  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default CheckoutScreen;