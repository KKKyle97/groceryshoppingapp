import React, {Component} from 'react';
import 'react-native-gesture-handler';
import {View, Button, StyleSheet, TouchableOpacity, Text} from 'react-native';
import CustomizedButton, {SignInButton} from '../components/CustomizedButton';
import {openDatabase} from 'react-native-sqlite-storage';
import HomeNewsSlides from '../components/HomeNewsSlides';

export default class HomeScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Welcome to Tesco!</Text>
        <HomeNewsSlides />
        <View style={styles.btn}>
          <SignInButton
            title="Browse all categories"
            onPress={() => {
              this.props.navigation.navigate('Category');
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },

  title: {
    fontSize: 32,
    textAlign: 'center',
    marginBottom: 30,
  },

  btn: {
    textAlign: 'center',
    marginTop: -250,
    height: 25,
    width: 210,
  },
});
