import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Alert, ActivityIndicator } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Picker} from '@react-native-community/picker';
import firestore from '@react-native-firebase/firestore';
import CustomizedButton from '../components/CustomizedButton';


export default class DeliveryInfoScreen extends Component {
  constructor() {
    super();
    this.firestoreRef = firestore().collection('delivery');
    this.state = {
      name: '',
      contact: '',
      address: '',
      paymentMethod: 'Online Banking',
      isLoading: false
    };
  }

  inputValueUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  addDelivery = () => {
    if(this.state.name === '' || this.state.contact === '' || this.state.address === '' || this.state.paymentMethod === '') {
      Alert.alert('Enter your delivery Info!')
    } else {
      this.setState({
        isLoading: true,
      })
      this.firestoreRef.doc('1').update({
          name: this.state.name,
          contact: this.state.contact,
          address: this.state.address,
          paymentMethod: this.state.paymentMethod,
      }).then((res) => {
        console.log('Info edited successfully!')
        this.setState({
          isLoading: false,
          contact: '',
          address: '',
          paymentMethod: ''
        });
        this.props.navigation.navigate('Checkout')
      })
      .catch(error => this.setState({ errorMessage: error.message }))
    }
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }
    return (
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <Text style={styles.label}>
          Name
        </Text>
        <TextInput
          style={styles.inputStyle}
          placeholder="Name"
          value={this.state.name}
          onChangeText={(val) => this.inputValueUpdate(val, 'name')}
        />
        <Text style={styles.label}>
          Contact Number
        </Text>
        <TextInput
          style={styles.inputStyle}
          placeholder="Contact"
          value={this.state.contact}
          onChangeText={(val) => this.inputValueUpdate(val, 'contact')}
        />
        <Text style={styles.label}>
          Address
        </Text>
        <TextInput
          style={styles.inputStyle}
          placeholder="Address"
          value={this.state.address}
          onChangeText={(val) => this.inputValueUpdate(val, 'address')}
        />
        <Text style={styles.label}>
          Payment Method
        </Text>
        <Picker style={styles.pickerStyle}  
          selectedValue={this.state.paymentMethod}  
          onValueChange={(val) => this.inputValueUpdate(val, 'paymentMethod')}
        >  
            <Picker.Item label="Cash on Delivery" value="Cash on Delivery" />  
            <Picker.Item label="Online Banking" value="Online Banking" />  
        </Picker>
        <CustomizedButton
          title="Confirm"
          onPress={() => this.addDelivery()}
        />
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: 35,
    backgroundColor: '#fff'
  },
  label: {
    flex: 1,
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 3,
    textAlignVertical: 'center',
  },
  inputStyle: {
    width: '100%',
    marginBottom: 20,
    paddingBottom: 15,
    alignSelf: "center",
    borderColor: "#ccc",
    borderBottomWidth: 1,
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  pickerStyle:{    
    width: "80%",  
    color: '#344953',  
    justifyContent: 'center',
    marginBottom: 30,  
  }  
});