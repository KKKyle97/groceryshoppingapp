import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  Button,
  TouchableHighlight,
} from 'react-native';
import {Avatar} from 'react-native-elements';
export default class ItemListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      groceries: [],
      title: this.props.route.params.title,
    };
  }
  // componentDidMount() {
  //   console.warn(this.props.route.params.title)
  //   let groceries = [
  //     {
  //       id: 1,
  //       title: 'Lipton Yellow Label Black Tea 100 Tea Bags x 2g (200g)',
  //       subtitle: 'RM13.00 ',
  //       mini: 'RM13.00/Each',
  //       avatar: require('C:/WAD/Assignment/groceryshoppingapp/Picture/Tea/Lipton_Yellow_Label_Black_Tea_100_Tea_Bags_2g_(200g).jpg'),
  //     },
  //     {
  //       id: 2,
  //       title: 'BOH Cameron Highlands Tea 100 Teabags x 2g (200g)',
  //       subtitle: 'RM13.99',
  //       mini: 'RM13.99/Each',
  //       avatar: require('C:/WAD/Assignment/groceryshoppingapp/Picture/Tea/BOH_Cameron_Highlands_Tea_100_Teabags_2g_(200g).jpg'),
  //     },
  //     {
  //       id: 3,
  //       title: 'BOH Cameron Highlands Tea 40 Potbags 80g',
  //       subtitle: 'RM4.99 ',
  //       mini: 'RM4.99/Each',
  //       avatar: require('C:/WAD/Assignment/groceryshoppingapp/Picture/Tea/BOH_Cameron_Highlands_Tea_40_Potbags_80g.jpg'),
  //     },
  //     {
  //       id: 4,
  //       title: 'Tesco Original Tea 80pcs 250g',
  //       subtitle: 'RM13.99',
  //       mini: 'RM13.99/Each',
  //       avatar: require('C:/WAD/Assignment/groceryshoppingapp/Picture/Tea/Tesco_Original_Tea_80pcs_250g.jpg'),
  //     },

  //   ];

  //   this.setState({
  //     groceries: groceries,
  //   })
  // }

  Render_Header = () => {
    var header = (
      <View style={styles.header_style}>
        <Text style={{textAlign: 'center', fontSize: 22, color: 'white'}}>
          Browse All Groceries
        </Text>
      </View>
    );
    return header;
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.groceries}
          showsVerticalScrollIndicator={false}
          renderItem={({item}) => (
            <TouchableHighlight underlayColor={'white'}>
              <View style={styles.item}>
                <Avatar
                  style={styles.image}
                  source={item.avatar}
                  size="medium"
                  activeOpacity={0.7}
                />
                <View style={{flex: 0.9}}>
                  <Text style={styles.itemTitle}>{item.title}</Text>
                  <Text style={styles.subtitle}>{item.subtitle}</Text>
                  <Text style={styles.mini}>{item.mini}</Text>
                </View>
                <View style={styles.fixToText}>
                  <Button color="#3369FF" title="Add" />
                </View>
              </View>
            </TouchableHighlight>
          )}
          ListHeaderComponent={this.Render_Header}
          stickyHeaderIndices={[0]}
          keyExtractor={(item) => {
            item.id.toString();
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#DADAE0',
  },
  subcontainer: {
    flex: 0.4,
    justifyContent: 'center',
    marginTop: 20,

    flexDirection: 'column',
  },
  item: {
    height: 100,
    paddingBottom: 15,
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: 'row',
    backgroundColor: 'white',
    marginBottom: 5,
    flex: 1,
  },
  itemTitle: {
    fontSize: 13,
    fontWeight: 'bold',
    marginHorizontal: 15,
    paddingTop: 5,
    color: '#3369FF',
    flex: 1,
    width: 200,
  },
  header_style: {
    width: '100%',
    height: 60,
    backgroundColor: '#3369FF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    paddingTop: 10,
    paddingBottom: 10,
    width: 70,
    height: 90,
  },
  subtitle: {
    marginHorizontal: 15,
    paddingTop: 5,
    fontSize: 18,
    fontWeight: 'bold',
  },
  mini: {
    marginHorizontal: 15,
    fontSize: 10,
    color: 'grey',
  },
  fixToText: {
    flexDirection: 'column',
    marginTop: 50,
    flex: 0.4,
    justifyContent: 'center',
  },
});
