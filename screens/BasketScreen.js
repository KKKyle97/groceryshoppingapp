import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import CustomizedButton, {SignInButton} from '../components/CustomizedButton';
import EmptyPanel from '../components/EmptyPanel';
import CartItem from '../components/CartItem';

export default class BasketScreen extends Component {
  constructor() {
    super();
    this.firestoreRef = firestore().collection('baskets');
    this.state = {
      isLoading: true,
      basketArr: [],
      totalItem: 0,
      totalPrice: 0,
    };
  }
  
  static navigationOptions = {
    title: 'Basket',
  };

  componentDidMount() {
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  getCollection = (querySnapshot) => {
    const basketArr = [];
    let totalAmount = 0;
    let totalPrice = 0;
    querySnapshot.forEach((res) => {
      const {name, amount, price, image} = res.data();
      totalAmount = totalAmount + amount;
      totalPrice = totalPrice + amount * price;
      basketArr.push({
        key: res.id,
        res,
        name,
        amount,
        price,
        image,
      });
    });
    this.setState({
      basketArr,
      isLoading: false,
      totalItem: totalAmount,
      totalPrice,
    });
  };

  selectFromBasket() {
    return false;
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.container}>
          <CustomizedButton title="Book a Slot" />
          <EmptyPanel
            logo="basket-outline"
            heading="Empty basket"
            message="Add products from your favorites or search for something specific"
          />
          <SignInButton title="Sign In" />
          <CustomizedButton title="Register" />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <View style={styles.subtotalContainer}>
          <View style={styles.number}>
            <Text>Items</Text>
            <Text>{this.state.totalItem}</Text>
          </View>
          <View style={styles.subtotal}>
            <Text>Guide Price</Text>
            <Text>RM{this.state.totalPrice}</Text>
          </View>
        </View>
        {this.state.basketArr.map((item, i) => {
          return (
            <CartItem
              key={i}
              name={item.name}
              img={item.image}
              price={item.price}
              amount={item.amount}
              addItem={() => {
                this.firestoreRef
                  .doc(item.key)
                  .update({amount: item.amount + 1});
              }}
              removeItem={() => {
                if (item.amount != 0) {
                  this.firestoreRef
                    .doc(item.key)
                    .update({amount: item.amount - 1});
                } else {
                  this.firestoreRef.doc(item.key).delete();
                }
              }}
            />
          );
        })}
        <View style={styles.stickToBottom}>
          <CustomizedButton title="Checkout" onPress={()=> {this.props.navigation.navigate('Checkout')}} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5F5F5',
  },
  stickToBottom: {
    justifyContent: 'flex-end',
    flex: 1,
  },
  subtotalContainer: {
    height: 50,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  number: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
  subtotal: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 5,
    alignItems: 'flex-end',
  },
});