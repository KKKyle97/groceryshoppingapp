import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import EmptyPanel from '../components/EmptyPanel';
import {SearchBar} from 'react-native-elements';

export default class SearchScreen extends Component {
  static navigationOptions = {
    /**
     * Specifies the text label that will be displayed in the drawer item.
     */ drawerLabel: 'Search all groceries',
  };

  state = {
    search: '',
  };

  updateSearch = (search) => {
    this.setState({search});
  };

  render() {
    const {search} = this.state;
    return (
      <View style={styles.container}>
        <SearchBar
          platform="android"
          placeholder="Search for a product"
          onChangeText={this.updateSearch}
          value={search}
        />

        <EmptyPanel
          logo="search"
          heading="Search groceries"
          message="What are you looking for?"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },

  title: {
    fontSize: 48,
    textAlign: 'center',
    margin: 20,
  },
});
