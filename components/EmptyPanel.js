import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import CircleLogo from './Circle';

class EmptyPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={styles.container}>
        <CircleLogo logo={this.props.logo} />
        <Text style={styles.heading}>{this.props.heading}</Text>
        <Text style={styles.message}>{this.props.message}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  heading: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  message: {
    textAlign: 'center',
    margin: 20,
  },
});

export default EmptyPanel;
