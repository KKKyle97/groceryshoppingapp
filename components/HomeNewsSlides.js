import React, { Component } from 'react';
import { Dimensions, Image, StyleSheet, View } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'local_db.db' });

const { width: screenWidth } = Dimensions.get('screen');

export default class HomeNewsSlides extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeSlide: 0,
      news: [],
    };

    db.transaction((tx) => {
      
      tx.executeSql('SELECT * FROM main_category', [], (tx, results) => {
        var temp = [];
        
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
          console.log(temp);
        }
        this.setState({
          news: temp,
        });
      });
    });
  }
  

  _renderItem({ item }) {
    return (
      <View style={styles.slide}>
        <Image
          style={styles.image}
          resizeMode="contain"
          source={{ uri:item.category_file}}
        />
      </View>
    );
  }
  
  pagination() {
    const { activeSlide } = this.state;
    return (
      <Pagination
        dotsLength={this.state.news.length}
        activeDotIndex={activeSlide}
        containerStyle={{
          backgroundColor: 'transparent',
          position: 'absolute',
          bottom: 0,
          alignSelf: 'center',
        }}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          backgroundColor: 'rgba(255, 255, 255, 0.92)',
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }

  render() {
    return (
      <View>
        <Carousel
          data={this.state.news}
          renderItem={this._renderItem}
          onSnapToItem={index => this.setState({ activeSlide: index })}
          sliderWidth={screenWidth}
          itemWidth={screenWidth}
          pinchGestureEnabled= { true }
          autoplay = {true}
          autoplayInterval = { 5000 }
          loop = { true }
        />
        {this.pagination()}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  slide: {
    marginTop: 10,
    height: screenWidth * 0.5,
  },
  image: {
    marginTop: 10,
    width: screenWidth,
    height: screenWidth * 0.5,
  },
});
