import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableNativeFeedback,
  Image,
} from 'react-native';
import AdjustItemButton from './AdjustItemButton';
import images from '../image';

class CartItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <View style={styles.imgContainer}>
            <Image style={styles.img} source={{uri: this.props.img}} />
          </View>
          <View style={styles.contentContainer}>
            <View style={styles.topContainer}>
              <Text style={styles.itemTitle}>{this.props.name}</Text>
            </View>
            <View style={styles.btmContainer}>
              <View style={styles.btmLeftContainer}>
                <Text style={{fontWeight: 'bold', fontSize: 18}}>
                  RM{this.props.price}
                </Text>
                <Text style={{fontSize: 12}}>RM{this.props.price}/Each</Text>
              </View>
              <View style={styles.btmRightContainer}>
                <AdjustItemButton
                  name="remove-circle-outline"
                  onPress={this.props.removeItem}
                />
                <Text
                  style={{marginHorizontal: 10, marginTop: 5, fontSize: 20}}>
                  {this.props.amount}
                </Text>
                <AdjustItemButton
                  name="add-circle-outline"
                  onPress={this.props.addItem}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 130,
    marginVertical: 10,
    paddingTop: 10,
    backgroundColor: 'white',
  },
  subContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  imgContainer: {
    flex: 1,
  },
  contentContainer: {
    flex: 3,
  },
  topContainer: {
    flex: 1,
    paddingHorizontal: 10,
  },
  btmContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  btmLeftContainer: {
    flex: 1,
  },
  btmRightContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  img: {
    width: 100,
    height: 100,
  },
  itemTitle: {
    flex: 3,
    paddingRight: 30,
    fontWeight: 'bold',
    color: 'darkblue',
    fontSize: 18,
  },
});

export default CartItem;
