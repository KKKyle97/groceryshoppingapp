import React, {Component} from 'react';
import {StyleSheet, View, TouchableNativeFeedback, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

class AdjustItemButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View>
        <TouchableNativeFeedback onPress={this.props.onPress}>
          <View>
            <Icon name={this.props.name} size={35} color={'blue'} />
          </View>
        </TouchableNativeFeedback>
      </View>
    );
  }
}

export default AdjustItemButton;
