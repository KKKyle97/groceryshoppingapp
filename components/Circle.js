import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

class CircleLogo extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.circle}>
          <Icon name={this.props.logo} size={100} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 30,
  },
  circle: {
    backgroundColor: '#F5F5F5',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    width: 150,
    height: 150,
  },
});

export default CircleLogo;
