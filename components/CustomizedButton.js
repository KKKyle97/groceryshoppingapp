import React, {Component} from 'react';
import {View, StyleSheet, Text, Button, TouchableHighlight} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

// customizable button with title option
class CustomizedButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={customizedButtonStyles.container}>
        <TouchableHighlight
          onPress={this.props.onPress}>
          <View style={customizedButtonStyles.btn}>
            <Text style={customizedButtonStyles.text}>{this.props.title}</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

const customizedButtonStyles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
  btn: {
    backgroundColor: 'white',
    padding: 10,
    alignItems: 'center',
    borderRadius: 20,
    borderColor: 'blue',
    borderWidth: 2,
  },
  text: {
    color: 'blue',
  },
});

// sign in button only not customizable
class SignInButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={signInButtonStyles.container}>
        <TouchableHighlight
          onPress={this.props.onPress}>
          <View style={signInButtonStyles.signInBtn}>
          <Text style={signInButtonStyles.signInText}>{this.props.title}</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

const signInButtonStyles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
  signInBtn: {
    backgroundColor: 'blue',
    padding: 10,
    alignItems: 'center',
    borderRadius: 20,
  },
  signInText: {
    color: 'white',
  },
});


export default CustomizedButton;
export {SignInButton};
