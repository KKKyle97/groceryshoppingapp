import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeScreen from './screens/HomeScreen';
import SearchScreen from './screens/SearchScreen';
import CategoryScreen from './screens/CategoryScreen';
import SubCategoryScreen from './screens/SubCategoryScreen';
import ItemListScreen from './screens/ItemListScreen';
import BasketScreen from './screens/BasketScreen';
import CheckoutScreen from './screens/CheckoutScreen';
import DeliveryInfoScreen from './screens/DeliveryInfoScreen';

const BasketStack = createStackNavigator();

function BasketStackScreen() {
  return (
    <BasketStack.Navigator>
      <BasketStack.Screen
        name="basket"
        component={BasketScreen}
        options={{
          title: 'Basket',
          headerStyle: {
            backgroundColor: '#2962ff',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <BasketStack.Screen
        name="Checkout"
        component={CheckoutScreen}
        options={{
          title: 'Checkout',
          headerStyle: {
            backgroundColor: '#2962ff',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <BasketStack.Screen
        name="Delivery"
        component={DeliveryInfoScreen}
        options={{
          title: 'Delivery',
          headerStyle: {
            backgroundColor: '#2962ff',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
    </BasketStack.Navigator>
  );
}

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function StackComponent() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: 'Home',
          headerStyle: {
            backgroundColor: '#2962ff',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="Category"
        component={CategoryScreen}
        options={{
          title: 'Browse All Categories',
          // headerStyle: {backgroundColor: '#3369FF'}, headerTitleStyle: {color: 'white'},
          // headerTintColor:'white'
        }}
      />
      <Stack.Screen
        name="SubCategory"
        component={SubCategoryScreen}
        options={({route}) => ({title: route.params.title})}
        // options={
        //   {headerStyle: {backgroundColor: '#3369FF'}, headerTitleStyle: {color: 'white'},
        //   headerTintColor:'white'}}
      />
      <Stack.Screen
        name="ItemList"
        component={ItemListScreen}
        options={({route}) => ({title: route.params.title})}
        // options={{
        //   title:'Items List',
        //   headerStyle: {backgroundColor: '#3369FF'}, headerTitleStyle: {color: 'white'},
        //   headerTintColor:'white'
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="Home"
        tabBarOptions={{
          activeTintColor: 'blue',
          inactiveTintColor: 'gray',
        }}>
        <Tab.Screen
          name="Home"
          component={StackComponent}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({color, size}) => (
              <Ionicons name="home-outline" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="Search"
          component={SearchScreen}
          options={{
            tabBarLabel: 'Search',
            tabBarIcon: ({color, size}) => (
              <Ionicons
                name="search-circle-outline"
                color={color}
                size={size}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Orders"
          component={HomeScreen}
          options={{
            tabBarLabel: 'Orders',
            tabBarIcon: ({color, size}) => (
              <Ionicons name="newspaper-outline" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="Basket"
          component={BasketStackScreen}
          options={{
            tabBarLabel: 'Basket',
            tabBarIcon: ({color, size}) => (
              <Ionicons name="basket-outline" color={color} size={size} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
